﻿using AppointmentModule.Factories;
using AppointmentModule.Models;
using AppointmentModule.Services;
using AppointmentModule.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using OntoMsg_Module.StateMachines;

namespace AppointmentModule.Controllers
{
    public class AppointmentController : AppointmentViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private clsLocalConfig localConfig;

        private ServiceAgentElastic serviceAgentElastic;
        private AppointmentFactory appointmentFactory;

        private clsOntologyItem oItemSended;

        private List<Appointment> appointments;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public AppointmentController()
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += AppointmentController_PropertyChanged;
        }

        private void AppointmentController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            if (e.PropertyName == nameof(KendoSchedulerEvent_EventToSave))
            {
                if (KendoSchedulerEvent_EventToSave != null)
                {
                    var result = serviceAgentElastic.SaveEvent(KendoSchedulerEvent_EventToSave, webSocketServiceAgent.oItemUser);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        var appointment = result.OList_Rel.First();
                        
                        KendoSchedulerEvent_EventToSave.taskId = appointment.GUID;
                        var appointmentItem = new Appointment(KendoSchedulerEvent_EventToSave, webSocketServiceAgent.oItemUser);
                        if (appointments == null)
                        {
                            appointments.Add(appointmentItem);
                        }

                        webSocketServiceAgent.SendPropertyChange(e.PropertyName);
                        webSocketServiceAgent.SendCommand("closeEventWindow");
                    }
                    else
                    {
                        webSocketServiceAgent.SendCommand("closeEventWindow");
                    }
                }
                return;
            }

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);
        }

        private void Initialize()
        {


            serviceAgentElastic = new ServiceAgentElastic(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;
            appointmentFactory = new AppointmentFactory(localConfig);
            appointmentFactory.PropertyChanged += AppointmentFactory_PropertyChanged;

            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            Text_View = translationController.Text_View;
        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            GetAppointmentsOfUser();
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void AppointmentFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(AppointmentFactory.Appointments))
            {
                appointments = appointmentFactory.Appointments;

                if (webSocketServiceAgent.IdEntryView == "9f966e30c4734b9d82a23ddf98bbeae0")
                {
                    var sessionFile = webSocketServiceAgent.RequestWriteStream(Guid.NewGuid().ToString() + ".json");

                    //var sessionFileUpdate = webSocketServiceAgent.FileSystemServiceAgent.RequestWriteStream(webSocketServiceAgent.DataText_SessionId, Guid.NewGuid().ToString() + ".json");
                    //var sessionFileCreate = webSocketServiceAgent.FileSystemServiceAgent.RequestWriteStream(webSocketServiceAgent.DataText_SessionId, Guid.NewGuid().ToString() + ".json");
                    //var sessionFileDestroy = webSocketServiceAgent.FileSystemServiceAgent.RequestWriteStream(webSocketServiceAgent.DataText_SessionId, Guid.NewGuid().ToString() + ".json");



                    var result = appointmentFactory.WriteSchedulerItems(appointments, sessionFile);
                    
                }
            }
            else if (e.PropertyName == nameof(AppointmentFactory.ResultWrite))
            {
                if (appointmentFactory.ResultWrite.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    Dictionary_SchedulerDataSource = appointmentFactory.GetSchedulerDataSource(appointmentFactory.SessionFile);
                }
            }
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ServiceAgentElastic.ResultAppointment))
            {
                var resultAppointment = serviceAgentElastic.ResultAppointment;
                if (resultAppointment.ResultAppointment.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var resultTask = appointmentFactory.GetAppointments(resultAppointment);
                }
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;




        }

       

       
        private void GetAppointmentsOfUser()
        {
            var appointmentFilter = new Appointment
            {
                Users = new List<SecurityModule.Models.User>
                {
                    new SecurityModule.Models.User
                    {
                        IdUser = webSocketServiceAgent.oItemUser.GUID,
                        NameUser = webSocketServiceAgent.oItemUser.Name
                    }
                }
            };

            var resultTask = serviceAgentElastic.GetAppointments(new List<Appointment> { appointmentFilter });
        }

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {

            }
            else if (e.PropertyName == NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                    if (changedItem.ViewItemId == "eventToSave")
                    {
                        KendoSchedulerEvent_EventToSave = Newtonsoft.Json.JsonConvert.DeserializeObject<KendoSchedulerEvent>(changedItem.ViewItemValue.ToString());
                    }
                    else if (changedItem.ViewItemId == "selectId")
                    {
                        if (!string.IsNullOrEmpty(changedItem.ViewItemValue.ToString()))
                        {
                            var oItem = serviceAgentElastic.GetOItem(changedItem.ViewItemValue.ToString(), localConfig.Globals.Type_Object);
                            var interModMessage = new InterServiceMessage
                            {
                                ChannelId = Channels.ParameterList,
                                OItems = new List<clsOntologyItem>
                                {
                                    oItem
                                }
                            };

                            webSocketServiceAgent.SendInterModMessage(interModMessage);
                        }
                        
                    }
                });
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ObjectArgument && webSocketServiceAgent.ObjectArgument != null)
            {
               

            }


        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {


            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.ParameterList)
            {

                var oItemMessage = message.OItems.LastOrDefault();
                if (oItemMessage == null) return;
                
                oItemSended = oItemMessage;
                if (appointments.Any())
                {
                    var appointmentItem = appointments.FirstOrDefault(app => app.IdAppointment == oItemSended.GUID);
                    if (appointmentItem == null) return;

                    DataText_SelectId = appointmentItem.UId;
                }

            }



        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
