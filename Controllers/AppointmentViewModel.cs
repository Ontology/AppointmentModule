﻿using AppointmentModule.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppointmentModule.Controllers
{
    public class AppointmentViewModel : ViewModelBase
    {
        private string text_View;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewText", ViewItemType = ViewItemType.Other)]
		public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(NotifyChanges.View_Text_View);

            }
        }

        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
		public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {
                if (istoggled_Listen == value) return;

                istoggled_Listen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Listen);

            }
        }

        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
		public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private Dictionary<string, object> dictionary_SchedulerDataSource;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "schedulerDataSource", ViewItemType = ViewItemType.Other)]
		public Dictionary<string, object> Dictionary_SchedulerDataSource
        {
            get { return dictionary_SchedulerDataSource; }
            set
            {
                if (dictionary_SchedulerDataSource == value) return;

                dictionary_SchedulerDataSource = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Dictionary_SchedulerDataSource);

            }
        }

        private KendoSchedulerEvent kendoschedulerevent_EventToSave;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "eventToSave", ViewItemType = ViewItemType.Other)]
		public KendoSchedulerEvent KendoSchedulerEvent_EventToSave
        {
            get { return kendoschedulerevent_EventToSave; }
            set
            {
                if (kendoschedulerevent_EventToSave == value) return;

                kendoschedulerevent_EventToSave = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_KendoSchedulerEvent_EventToSave);

            }
        }

        private string datatext_SelectId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "selectId", ViewItemType = ViewItemType.Other)]
		public string DataText_SelectId
        {
            get { return datatext_SelectId; }
            set
            {
                if (datatext_SelectId == value) return;

                datatext_SelectId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_SelectId);

            }
        }


    }
}
