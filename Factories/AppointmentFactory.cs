﻿using AppointmentModule.Models;
using AppointmentModule.Services;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using PartnerModule.Connectors;
using PartnerModule.Models;
using SecurityModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppointmentModule.Factories
{
    public class AppointmentFactory : NotifyPropertyChange
    {
        private object appointmentLocker = new object();
        private object writeLocker = new object();

        private SessionFile sessionFile;
        public SessionFile SessionFile
        {
            get { return sessionFile; }
            set
            {
                sessionFile = value;
            }
        }
        private clsLocalConfig localConfig;

        private PartnerConnector partnerConnector;

        private List<Appointment> appointments;
        public List<Appointment> Appointments
        {
            get
            {
                lock(appointmentLocker)
                {
                    return appointments;
                }
                
            }
            set
            {
                lock(appointmentLocker)
                {
                    appointments = value;
                }

                RaisePropertyChanged(nameof(Appointments));
            }
        }

        private clsOntologyItem resultWrite;
        public clsOntologyItem ResultWrite
        {
            get
            {
                lock (appointmentLocker)
                {
                    return resultWrite;
                }

            }
            set
            {
                lock (appointmentLocker)
                {
                    resultWrite = value;
                }

                RaisePropertyChanged(nameof(ResultWrite));
            }
        }

        public async Task<List<Appointment>> GetAppointments(AppointmentResult appointmentResult)
        {
            var appointments = (from appointmentToUser in appointmentResult.AppointmentsToUsers.GroupBy(appToUser => new { IdAppointment = appToUser.ID_Object, NameAppointment = appToUser.Name_Object})
                                join startItem in appointmentResult.AppointmentAttributes.Where(attr => attr.ID_AttributeType == localConfig.OItem_attribute_start.GUID)
                                     on appointmentToUser.Key.IdAppointment equals startItem.ID_Object into startItems
                                from startItem in startItems.DefaultIfEmpty()
                                join endItem in appointmentResult.AppointmentAttributes.Where(attr => attr.ID_AttributeType == localConfig.OItem_attribute_ende.GUID)
                                     on appointmentToUser.Key.IdAppointment equals endItem.ID_Object into endItems
                                from endItem in endItems.DefaultIfEmpty()
                                join refItem in appointmentResult.AppointmentsToRefs on appointmentToUser.Key.IdAppointment equals refItem.ID_Object into refItems
                                from refItem in refItems.DefaultIfEmpty()
                                select new Appointment
                                {
                                    IdAppointment = appointmentToUser.Key.IdAppointment,
                                    NameAppointment = appointmentToUser.Key.NameAppointment,
                                    IdAttributeStart = startItem != null ? startItem.ID_Attribute : null,
                                    Start = startItem != null ? startItem.Val_Date : null,
                                    IdAttributeEnd = endItem != null ? endItem.ID_Attribute : null,
                                    End = endItem != null ? endItem.Val_Date : null,
                                    IdRef = refItem != null ? refItem.ID_Other : null,
                                    NameRef = refItem != null ? refItem.Name_Other : null,
                                    UId = Guid.NewGuid().ToString()
                                }).ToList().ToList();

            var contractors = appointmentResult.AppointmentPartners.Where(partner => partner.ID_RelationType == localConfig.OItem_relationtype_belonging_contractor.GUID).ToList();
            var contractees = appointmentResult.AppointmentPartners.Where(partner => partner.ID_RelationType == localConfig.OItem_relationtype_belonging_contractee.GUID).ToList();
            var watchers = appointmentResult.AppointmentPartners.Where(partner => partner.ID_RelationType == localConfig.OItem_relationtype_belonging_watchers.GUID).ToList();

            var addressesPre = appointmentResult.AppointmentAddresses.Select(addr => new clsOntologyItem
            {
                GUID = addr.ID_Other,
                Name = addr.Name_Other,
                GUID_Parent = localConfig.OItem_type_address.GUID,
                Type = localConfig.Globals.Type_Object
            }).ToList();

            var addressResultTask = partnerConnector.GetAddressFull(addressesPre);
            addressResultTask.Wait();
            var addresses = (from addressRaw in appointmentResult.AppointmentAddresses
                             join addressFull in addressResultTask.Result on addressRaw.ID_Other equals addressFull.Id
                             select new { addressRaw, addressFull }).ToList();

            appointments.ForEach(appointment =>
            {
                appointment.Addresses = addresses.Where(addr => addr.addressRaw.ID_Object == appointment.IdAppointment).Select(addr => addr.addressFull).ToList();

                appointment.Contractors = contractors.Where(contractor => contractor.ID_Object == appointment.IdAppointment).Select(partner => new Partner
                {
                    Id = partner.ID_Other,
                    Name = partner.Name_Other
                }).ToList();

                appointment.Contractees = contractees.Where(contractee => contractee.ID_Object == appointment.IdAppointment).Select(partner => new Partner
                {
                    Id = partner.ID_Other,
                    Name = partner.Name_Other
                }).ToList();

                appointment.Watchers = watchers.Where(watcher => watcher.ID_Object == appointment.IdAppointment).Select(partner => new Partner
                {
                    Id = partner.ID_Other,
                    Name = partner.Name_Other
                }).ToList();

                appointment.Rooms = appointmentResult.AppointmentRooms.Where(room => room.ID_Object == appointment.IdAppointment).Select(room => new Room
                {
                    IdRoom = room.ID_Other,
                    NameRoom = room.Name_Other
                }).ToList();

                appointment.Locations = appointmentResult.AppointmentLocations.Where(loc => loc.ID_Object == appointment.IdAppointment).Select(loc => new Location
                {
                    IdLocation = loc.ID_Other,
                    NameLocation = loc.Name_Other
                }).ToList();

                appointment.Users = appointmentResult.AppointmentsToUsers.Where(usr => usr.ID_Object == appointment.IdAppointment).Select(usr => new User
                {
                    IdUser = usr.ID_Other,
                    NameUser = usr.Name_Other
                }).ToList();

            });

            Appointments = appointments;

            return Appointments;
        }

        public async Task<clsOntologyItem> WriteSchedulerItems(List<Appointment> appointments, SessionFile sessionFile)
        {
            this.sessionFile = sessionFile;
            var result = localConfig.Globals.LState_Success.Clone();

            var groupedItems = appointments.GroupBy(appointment => new
            {
                appointment.IdAppointment,
                appointment.NameAppointment,
                appointment.Start,
                appointment.End,
                appointment.Users.First().IdUser,
                appointment.Users.First().NameUser,
                appointment.UId
            }).ToList();
            lock(writeLocker)
            {
                using (sessionFile.StreamWriter)
                {
                    using (var jsonWriter = new Newtonsoft.Json.JsonTextWriter(sessionFile.StreamWriter))
                    {
                        jsonWriter.WriteStartArray();
                        groupedItems.ForEach(appointment =>
                        {
                            jsonWriter.WriteStartObject();

                            jsonWriter.WritePropertyName(nameof(Appointment.IdAppointment));
                            jsonWriter.WriteValue(appointment.Key.IdAppointment);

                            jsonWriter.WritePropertyName(nameof(Appointment.NameAppointment));
                            jsonWriter.WriteValue(appointment.Key.NameAppointment);

                            jsonWriter.WritePropertyName(nameof(Appointment.Start));
                            jsonWriter.WriteValue(appointment.Key.Start);

                            jsonWriter.WritePropertyName(nameof(Appointment.End));
                            jsonWriter.WriteValue(appointment.Key.End);

                            jsonWriter.WritePropertyName(nameof(User.IdUser));
                            jsonWriter.WriteValue(appointment.Key.IdUser);

                            jsonWriter.WritePropertyName(nameof(User.NameUser));
                            jsonWriter.WriteValue(appointment.Key.NameUser);

                            jsonWriter.WritePropertyName("uid");
                            jsonWriter.WriteValue(appointment.Key.UId);

                            //jsonWriter.WritePropertyName("TimeZone");
                            //jsonWriter.WriteValue("Europa/Berlin");

                            jsonWriter.WriteEndObject();
                        });
                        jsonWriter.WriteEndArray();
                    }
                }
            }

            ResultWrite = result;
            return result;
        }

        public Dictionary<string, object> GetSchedulerDataSource(SessionFile sessionFile)
        {
            var dictResult = new Dictionary<string, object>();

            var dictTransport = new Dictionary<string, object>();
            var dictRead = new Dictionary<string, object>();

            dictRead.Add("url", sessionFile.FileUri.AbsoluteUri);
            dictRead.Add("dataType", "json");

            dictTransport.Add("read", dictRead);

            dictResult.Add("transport", dictTransport);

            var dictSchema = new Dictionary<string, object>();
            dictResult.Add("schema", dictSchema);
            //dictSchema.Add("timezone", "Europa/Berlin");

            var dictModel = new Dictionary<string, object>();
            dictSchema.Add("model", dictModel);
            var dictFields = new Dictionary<string, object>();
            dictModel.Add("id", "taskId");
            dictModel.Add("fields", dictFields);

            var dictTask = new Dictionary<string, object>();
            dictTask.Add("from", nameof(Appointment.IdAppointment));
            dictTask.Add("type", "string");

            dictFields.Add("taskId", dictTask);

            var dictTitle = new Dictionary<string, object>();
            dictTitle.Add("from", nameof(Appointment.NameAppointment));
            dictTitle.Add("defaultValue", "No title");

            var dictValidation = new Dictionary<string, object>();
            dictValidation.Add("required", true);

            dictTitle.Add("validation", dictValidation);

            dictFields.Add("title", dictTitle);

            var dictField1 = new Dictionary<string, object>();

            dictField1.Add("type", "date");
            dictField1.Add("from", nameof(Appointment.Start));
            dictFields.Add("start", dictField1);

            var dictField2 = new Dictionary<string, object>();

            dictField2.Add("type", "date");
            dictField2.Add("from", nameof(Appointment.End));
            dictFields.Add("end", dictField2);

            //var dictField3 = new Dictionary<string, object>();
            //dictField3.Add("from", "TimeZone");
            //dictFields.Add("startTimezone", dictField3);

            //var dictField4 = new Dictionary<string, object>();
            //dictField4.Add("from", "TimeZone");
            //dictFields.Add("endTimezone", dictField4);

            var dictOwner = new Dictionary<string, object>();
            dictOwner.Add("from", nameof(User.IdUser));

            dictFields.Add("ownerId", dictOwner);

            return dictResult;
        }

        public AppointmentFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            partnerConnector = new PartnerConnector(localConfig.Globals);
        }

    }
}
