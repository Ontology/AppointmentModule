﻿using AppointmentModule.Notifications;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using OntoWebCore.Models;
using PartnerModule.Models;
using SecurityModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppointmentModule.Models
{
    public class Appointment : ViewModelBase
    {
        private string nameRef;
        public string NameRef
        {
            get { return nameRef; }
            set
            {
                if (nameRef == value) return;

                nameRef = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_NameRef);

            }
        }

        private string idRef;
        public string IdRef
        {
            get { return idRef; }
            set
            {
                if (idRef == value) return;

                idRef = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IdRef);

            }
        }

        private string idAppointment;
		public string IdAppointment
        {
            get { return idAppointment; }
            set
            {
                if (idAppointment == value) return;

                idAppointment = value;

                RaisePropertyChanged(NotifyChanges.Appointment_IdAppointment);

            }
        }

        private string nameAppointment;
		public string NameAppointment
        {
            get { return nameAppointment; }
            set
            {
                if (nameAppointment == value) return;

                nameAppointment = value;

                RaisePropertyChanged(NotifyChanges.Appointment_NameAppointment);

            }
        }

        private string idAttributeStart;
		public string IdAttributeStart
        {
            get { return idAttributeStart; }
            set
            {
                if (idAttributeStart == value) return;

                idAttributeStart = value;

                RaisePropertyChanged(NotifyChanges.Appointment_IdAttributeStart);

            }
        }

        private DateTime? start;
        public DateTime? Start
        {
            get { return start; }
            set
            {
                if (start == value) return;

                start = value;

                RaisePropertyChanged(NotifyChanges.Appointment_Start);

            }
        }

        private string idAttributeEnd;
        public string IdAttributeEnd
        {
            get { return idAttributeEnd; }
            set
            {
                if (idAttributeEnd == value) return;

                idAttributeEnd = value;

                RaisePropertyChanged(NotifyChanges.Appointment_IdAttributeEnd);

            }
        }

        private DateTime? end;
        public DateTime? End
        {
            get { return end; }
            set
            {
                if (end == value) return;

                end = value;

                RaisePropertyChanged(NotifyChanges.Appointment_End);

            }
        }

        private List<Room> rooms;
        public List<Room> Rooms
        {
            get { return rooms; }
            set
            {
                if (rooms == value) return;

                rooms = value;

                RaisePropertyChanged(NotifyChanges.Appointment_Rooms);

            }
        }

        private List<Partner> watchers;
        public List<Partner> Watchers
        {
            get { return watchers; }
            set
            {
                if (watchers == value) return;

                watchers = value;

                RaisePropertyChanged(NotifyChanges.Appointment_Watchers);

            }
        }

        private List<User> users;
        public List<User> Users
        {
            get { return users; }
            set
            {
                if (users == value) return;

                users = value;

                RaisePropertyChanged(NotifyChanges.Appointment_Users);

            }
        }

        private List<Partner> contractors;
        public List<Partner> Contractors
        {
            get { return contractors; }
            set
            {
                if (contractors == value) return;

                contractors = value;

                RaisePropertyChanged(NotifyChanges.Appointment_Contractors);

            }
        }

        private List<Partner> contractees;
        public List<Partner> Contractees
        {
            get { return contractees; }
            set
            {
                if (contractees == value) return;

                contractees = value;

                RaisePropertyChanged(NotifyChanges.Appointment_Contractees);

            }
        }

        private List<Address> addresses;
        public List<Address> Addresses
        {
            get { return addresses; }
            set
            {
                if (addresses == value) return;

                addresses = value;

                RaisePropertyChanged(NotifyChanges.Appointment_Addresses);

            }
        }

        private List<Location> locations;
        public List<Location> Locations
        {
            get { return locations; }
            set
            {
                if (locations == value) return;

                locations = value;

                RaisePropertyChanged(NotifyChanges.Appointment_Locations);

            }
        }

        public clsOntologyItem OItemAppointment { get; set; }
        public clsOntologyItem OItemRef { get; set; }
        public string UId { get; set; }

        public Appointment()
        {

        }

        public Appointment(KendoSchedulerEvent schedulerEvent, clsOntologyItem userItem)
        {
            Start = schedulerEvent.start;
            End = schedulerEvent.end;
            Users = new List<User>()
            {
                new User
                {
                    IdUser = userItem.GUID,
                    NameUser = userItem.Name
                }
            };

            IdAppointment = schedulerEvent.taskId.ToString();
            NameAppointment = schedulerEvent.title;
            UId = schedulerEvent.uid;
        }
    }
}
