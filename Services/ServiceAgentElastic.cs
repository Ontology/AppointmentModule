﻿using AppointmentModule.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using OntoWebCore.Models;
using SecurityModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppointmentModule.Services
{
    public class ServiceAgentElastic : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private clsTransaction transaction;
        public clsRelationConfig relationConfig;

        private AppointmentResult resultAppointment;

        private object lockService = new object();

        public AppointmentResult ResultAppointment
        {
            get
            {
                lock(lockService)
                {
                    return resultAppointment;
                }
                
            }
            set
            {
                lock(lockService)
                {
                    resultAppointment = value;
                }
                
                RaisePropertyChanged(nameof(ResultAppointment));
            }
        }

        public async Task<AppointmentResult> GetAppointments(List<Appointment> filter)
        {

            var _resultAppointments = new AppointmentResult
            {
                ResultAppointment = localConfig.Globals.LState_Success.Clone()
            };


            var result = localConfig.Globals.LState_Success.Clone();
            result = GetAppointmentsOfUsers(filter, _resultAppointments);
            _resultAppointments.ResultAppointment = result;


            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultAppointment = _resultAppointments;
                return ResultAppointment;
            }
            
            result = GetAppointmentsOfRefs(filter, _resultAppointments);            

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultAppointment = _resultAppointments;
                return ResultAppointment;
            }

            result = GetAppointmentAttributes(filter, _resultAppointments);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultAppointment = _resultAppointments;
                return ResultAppointment;
            }

            result = GetAppointmentPartners(filter, _resultAppointments);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultAppointment = _resultAppointments;
                return ResultAppointment;
            }

            result = GetAppointmentAddresses(filter, _resultAppointments);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultAppointment = _resultAppointments;
                return ResultAppointment;
            }

            result = GetAppointmentRooms(filter, _resultAppointments);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultAppointment = _resultAppointments;
                return ResultAppointment;
            }

            result = GetAppointmentLocations(filter, _resultAppointments);
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultAppointment = _resultAppointments;
                return ResultAppointment;
            }

            ResultAppointment = _resultAppointments;
            return _resultAppointments;
        }

        private clsOntologyItem GetAppointmentsOfUsers(List<Appointment> filter, AppointmentResult _resultAppointments)
        {
            var serviceReader = new OntologyModDBConnector(localConfig.Globals);


            var result = localConfig.Globals.LState_Success.Clone();
                
            var searchUsers = new List<clsObjectRel>();
            // Ref-Filter is provided
            if (filter.Any(appointment => appointment.Users != null))
            {
                var users = filter.Where(filterItem => filterItem.Users != null && filterItem.Users.Any()).SelectMany(filterItem => filterItem.Users);
                searchUsers = users.Select(user => new clsObjectRel
                {
                    ID_Other = user.IdUser,
                    Name_Other = user.NameUser,
                    ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                    ID_Parent_Object = localConfig.OItem_type_appointment.GUID
                }).ToList();
            }
            else
            {
                // Readed Appointments present

                searchUsers = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Other = localConfig.OItem_type_user.GUID,
                        ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                        ID_Parent_Object = localConfig.OItem_type_appointment.GUID
                    }
                };
            }    
                
                
            if (searchUsers.Any())
            {
                result = serviceReader.GetDataObjectRel(searchUsers);
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    _resultAppointments.AppointmentsToUsers = serviceReader.ObjectRels;
                }
            }
            

            return result;
        }

        private clsOntologyItem GetAppointmentsOfRefs(List<Appointment> filter, AppointmentResult _resultAppointments)
        {
            var serviceReader = new OntologyModDBConnector(localConfig.Globals);


            var result = localConfig.Globals.LState_Success.Clone();

            var searchRefs = new List<clsObjectRel>();
            // Ref-Filter is provided
            if (filter.Any(appointment => !string.IsNullOrEmpty(appointment.IdRef) || !string.IsNullOrEmpty(appointment.NameRef)))
            {
                var refs = filter.Where(filterItem => !string.IsNullOrEmpty(filterItem.IdRef) || !string.IsNullOrEmpty(filterItem.NameRef));
                searchRefs = (from userToAppointment in _resultAppointments.AppointmentsToUsers
                              from refItem in refs
                              select new clsObjectRel
                              {
                                ID_Other = refItem.IdRef,
                                Name_Other = refItem.NameRef,
                                ID_RelationType = localConfig.OItem_relationtype_belonging_resource.GUID,
                                ID_Object = userToAppointment.ID_Object
                            }).ToList();
            }
            else
            {
                searchRefs = _resultAppointments.AppointmentsToUsers.Select(userRel => new clsObjectRel
                {
                    ID_Object = userRel.ID_Object,
                    ID_RelationType = localConfig.OItem_relationtype_belonging_resource.GUID
                }).ToList();
                    
            }

            if (searchRefs.Any())
            {
                result = serviceReader.GetDataObjectRel(searchRefs);
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    _resultAppointments.AppointmentsToRefs = serviceReader.ObjectRels;
                }
            }

                
           

            return result;
        }

        public clsOntologyItem GetAppointmentAttributes(List<Appointment> filter, AppointmentResult _resultAppointments)
        {
            var serviceReader = new OntologyModDBConnector(localConfig.Globals);


            var result = localConfig.Globals.LState_Success.Clone();

            var searchAtts = new List<clsObjectAtt>();
            // Ref-Filter is provided
            if (filter.Any(appointment => appointment.Start != null || appointment.End != null))
            {
                searchAtts = filter.Where(filterItem => filterItem.Start != null).Select(filterItem => new clsObjectAtt
                {
                    ID_Class = localConfig.OItem_type_appointment.GUID,
                    Val_Date = filterItem.Start
                }).ToList();

                searchAtts.AddRange(filter.Where(filterItem => filterItem.End != null).Select(filterItem => new clsObjectAtt
                {
                    ID_Class = localConfig.OItem_type_appointment.GUID,
                    Val_Date = filterItem.End
                }));
                
            }
            else
            {
                searchAtts = _resultAppointments.AppointmentsToUsers.Select(userRel => new clsObjectAtt
                {
                    ID_Object = userRel.ID_Object,
                    ID_AttributeType = localConfig.OItem_attribute_start.GUID
                }).ToList();

                searchAtts.AddRange(_resultAppointments.AppointmentsToUsers.Select(userRel => new clsObjectAtt
                {
                    ID_Object = userRel.ID_Object,
                    ID_AttributeType = localConfig.OItem_attribute_ende.GUID
                }));

            }

            if (searchAtts.Any())
            {
                result = serviceReader.GetDataObjectAtt(searchAtts);
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    _resultAppointments.AppointmentAttributes = serviceReader.ObjAtts;
                }
            }




            return result;
        }

        private clsOntologyItem GetAppointmentPartners(List<Appointment> filter, AppointmentResult _resultAppointments)
        {
            var serviceReader = new OntologyModDBConnector(localConfig.Globals);


            var result = localConfig.Globals.LState_Success.Clone();

            var searchPartners = new List<clsObjectRel>();
            // Ref-Filter is provided
            if (filter.Any(appointment => appointment.Contractees != null))
            {
                var contractees = filter.Where(filterItem => filterItem.Contractees != null).SelectMany(filterItem => filterItem.Contractees);
                searchPartners = (from userToAppointment in _resultAppointments.AppointmentsToUsers
                              from contractee in contractees
                              select new clsObjectRel
                              {
                                  ID_Other = contractee.Id,
                                  Name_Other = contractee.Name,
                                  ID_RelationType = localConfig.OItem_relationtype_belonging_contractee.GUID,
                                  ID_Object = userToAppointment.ID_Object
                              }).ToList();

                var contractors = filter.Where(filterItem => filterItem.Contractors != null).SelectMany(filterItem => filterItem.Contractors);
                searchPartners.AddRange(from userToAppointment in _resultAppointments.AppointmentsToUsers
                                        from contractor in contractors
                                        select new clsObjectRel
                                        {
                                            ID_Other = contractor.Id,
                                            Name_Other = contractor.Name,
                                            ID_RelationType = localConfig.OItem_relationtype_belonging_contractor.GUID,
                                            ID_Object = userToAppointment.ID_Object
                                        });

                var watchers = filter.Where(filterItem => filterItem.Watchers != null).SelectMany(filterItem => filterItem.Watchers);
                searchPartners.AddRange(from userToAppointment in _resultAppointments.AppointmentsToUsers
                                        from watcher in watchers
                                        select new clsObjectRel
                                        {
                                            ID_Other = watcher.Id,
                                            Name_Other = watcher.Name,
                                            ID_RelationType = localConfig.OItem_relationtype_belonging_contractor.GUID,
                                            ID_Object = userToAppointment.ID_Object
                                        });
            }
            else
            {
                searchPartners = _resultAppointments.AppointmentsToUsers.Select(userRel => new clsObjectRel
                {
                    ID_Object = userRel.ID_Object,
                    ID_RelationType = localConfig.OItem_relationtype_belonging_contractee.GUID,
                    ID_Parent_Other = localConfig.OItem_type_partner.GUID
                }).ToList();

                searchPartners.AddRange(_resultAppointments.AppointmentsToUsers.Select(userRel => new clsObjectRel
                {
                    ID_Object = userRel.ID_Object,
                    ID_RelationType = localConfig.OItem_relationtype_belonging_contractor.GUID,
                    ID_Parent_Other = localConfig.OItem_type_partner.GUID
                }));

                searchPartners.AddRange(_resultAppointments.AppointmentsToUsers.Select(userRel => new clsObjectRel
                {
                    ID_Object = userRel.ID_Object,
                    ID_RelationType = localConfig.OItem_relationtype_belonging_watchers.GUID,
                    ID_Parent_Other = localConfig.OItem_type_partner.GUID
                }));

            }

            if (searchPartners.Any())
            {
                result = serviceReader.GetDataObjectRel(searchPartners);
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    _resultAppointments.AppointmentPartners = serviceReader.ObjectRels;
                }
            }

            return result;
        }

        private clsOntologyItem GetAppointmentAddresses(List<Appointment> filter, AppointmentResult _resultAppointments)
        {
            var serviceReader = new OntologyModDBConnector(localConfig.Globals);


            var result = localConfig.Globals.LState_Success.Clone();

            var searchAddresses = new List<clsObjectRel>();
            // Ref-Filter is provided
            if (filter.Any(appointment => appointment.Addresses != null))
            {
                var addresses = filter.Where(filterItem => filterItem.Addresses != null).SelectMany(filterItem => filterItem.Addresses) ;
                searchAddresses = (from userToAppointment in _resultAppointments.AppointmentsToUsers
                                   from address in addresses
                                   select new clsObjectRel
                                  {
                                      ID_Other = address.Id,
                                      Name_Other = address.Name,
                                      ID_RelationType = localConfig.OItem_relationtype_located_at.GUID,
                                      ID_Object = userToAppointment.ID_Object
                                  }).ToList();
            }
            else
            {
                searchAddresses = _resultAppointments.AppointmentsToUsers.Select(userRel => new clsObjectRel
                {
                    ID_Object = userRel.ID_Object,
                    ID_RelationType = localConfig.OItem_relationtype_located_at.GUID,
                    ID_Parent_Other = localConfig.OItem_type_address.GUID
                }).ToList();

            }

            if (searchAddresses.Any())
            {
                result = serviceReader.GetDataObjectRel(searchAddresses);
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    _resultAppointments.AppointmentAddresses = serviceReader.ObjectRels;
                }
            }

            return result;
        }

        private clsOntologyItem GetAppointmentRooms(List<Appointment> filter, AppointmentResult _resultAppointments)
        {
            var serviceReader = new OntologyModDBConnector(localConfig.Globals);


            var result = localConfig.Globals.LState_Success.Clone();

            var searchRooms = new List<clsObjectRel>();
            // Ref-Filter is provided
            if (filter.Any(appointment => appointment.Rooms != null))
            {
                var rooms = filter.Where(filterItem => filterItem.Rooms != null).SelectMany(filterItem => filterItem.Rooms);
                searchRooms = (from userToAppointment in _resultAppointments.AppointmentsToUsers
                                   from room in rooms
                                   select new clsObjectRel
                                   {
                                       ID_Other = room.IdRoom,
                                       Name_Other = room.NameRoom,
                                       ID_RelationType = localConfig.OItem_relationtype_located_at.GUID,
                                       ID_Object = userToAppointment.ID_Object
                                   }).ToList();
            }
            else
            {
                searchRooms = _resultAppointments.AppointmentsToUsers.Select(userRel => new clsObjectRel
                {
                    ID_Object = userRel.ID_Object,
                    ID_RelationType = localConfig.OItem_relationtype_located_at.GUID,
                    ID_Parent_Other = localConfig.OItem_type_raum.GUID
                }).ToList();

            }

            if (searchRooms.Any())
            {
                result = serviceReader.GetDataObjectRel(searchRooms);
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    _resultAppointments.AppointmentRooms = serviceReader.ObjectRels;
                }
            }

            return result;
        }

        private clsOntologyItem GetAppointmentLocations(List<Appointment> filter, AppointmentResult _resultAppointments)
        {
            var serviceReader = new OntologyModDBConnector(localConfig.Globals);


            var result = localConfig.Globals.LState_Success.Clone();

            var searchLocations = new List<clsObjectRel>();
            // Ref-Filter is provided
            if (filter.Any(appointment => appointment.Locations != null))
            {
                var locations = filter.Where(filterItem => filterItem.Locations != null).SelectMany(filterItem => filterItem.Locations);
                searchLocations = (from userToAppointment in _resultAppointments.AppointmentsToUsers
                               from location in locations
                               select new clsObjectRel
                               {
                                   ID_Other = location.IdLocation,
                                   Name_Other = location.NameLocation,
                                   ID_RelationType = localConfig.OItem_relationtype_located_at.GUID,
                                   ID_Object = userToAppointment.ID_Object
                               }).ToList();
            }
            else
            {
                searchLocations = _resultAppointments.AppointmentsToUsers.Select(userRel => new clsObjectRel
                {
                    ID_Object = userRel.ID_Object,
                    ID_RelationType = localConfig.OItem_relationtype_located_at.GUID,
                    ID_Parent_Other = localConfig.OItem_type_ort.GUID
                }).ToList();

            }

            if (searchLocations.Any())
            {
                result = serviceReader.GetDataObjectRel(searchLocations);
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    _resultAppointments.AppointmentLocations = serviceReader.ObjectRels;
                }
            }

            return result;
        }

        public clsOntologyItem SaveEvent(KendoSchedulerEvent schedulerEvent, clsOntologyItem oItemUser)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var result = localConfig.Globals.LState_Success.Clone();

            transaction.ClearItems();

            clsOntologyItem appointment = null;

            if (string.IsNullOrEmpty(schedulerEvent.taskId.ToString()))
            {
                appointment = new clsOntologyItem
                {
                    GUID = localConfig.Globals.NewGUID,
                    Name = schedulerEvent.title,
                    GUID_Parent = localConfig.OItem_type_appointment.GUID,
                    Type = localConfig.Globals.Type_Object
                };

                result = transaction.do_Transaction(appointment);
                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return result;
                }

                

            }
            else
            {
                appointment = dbReader.GetOItem(schedulerEvent.taskId.ToString(), localConfig.Globals.Type_Object);

                if (appointment == null)
                {
                    return localConfig.Globals.LState_Error.Clone();
                }
            }

            var start = relationConfig.Rel_ObjectAttribute(appointment, localConfig.OItem_attribute_start, schedulerEvent.start);

            result = transaction.do_Transaction(start, boolRemoveAll: true);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                transaction.rollback();
                return result;
            }

            var end = relationConfig.Rel_ObjectAttribute(appointment, localConfig.OItem_attribute_ende, schedulerEvent.end);

            result = transaction.do_Transaction(end, boolRemoveAll: true);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                transaction.rollback();
                return result;
            }

            var userRel = relationConfig.Rel_ObjectRelation(appointment, oItemUser, localConfig.OItem_relationtype_belongsto);

            result = transaction.do_Transaction(userRel, boolRemoveAll: false);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                transaction.rollback();
                return result;
            }

            result.OList_Rel = new List<clsOntologyItem>();
            result.OList_Rel.Add(appointment);
            return result;
        }

        public clsOntologyItem GetOItem(string id, string type)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);
            return dbReader.GetOItem(id, type);
        }
        

        public ServiceAgentElastic(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        public void Initialize()
        {
            transaction = new clsTransaction(localConfig.Globals);
            relationConfig = new clsRelationConfig(localConfig.Globals);
        }
    }


    public class AppointmentResult
    {
        public clsOntologyItem ResultAppointment { get; set; }
        public List<clsObjectRel> AppointmentsToRefs { get; set; }
        public List<clsObjectRel> AppointmentsToUsers { get; set; }
        public List<clsObjectAtt> AppointmentAttributes { get; set; }
        public List<clsObjectRel> AppointmentPartners { get; set; }
        public List<clsObjectRel> AppointmentAddresses { get; set; }
        public List<clsObjectRel> AppointmentRooms { get; set; }
        public List<clsObjectRel> AppointmentLocations { get; set; }

        public AppointmentResult()
        {

        }
    }
   
}
